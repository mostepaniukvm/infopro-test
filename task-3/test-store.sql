-- Host: 127.0.0.1:3306
-- Generation Time: May 17, 2018 at 12:37 PM
-- Server version: 5.7.20
-- PHP Version: 7.1.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `test_store`
--
CREATE DATABASE IF NOT EXISTS `test_store` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `test_store`;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Product name.',
  `price` decimal(10,0) NOT NULL COMMENT 'Price expressed in Euros.',
  `stock` int(11) NOT NULL COMMENT 'Quantity available of product packages.',
  `weight` decimal(10,0) NOT NULL COMMENT 'Weight of package espressed in grams.',
  `producer` varchar(255) NOT NULL COMMENT 'Producer name.',
  `country` varchar(2) NOT NULL COMMENT 'Country code in ISO2 format.'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Truncate table before insert `products`
--

TRUNCATE TABLE `products`;
--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `price`, `stock`, `weight`, `producer`, `country`) VALUES
(1, 'Carrots', '2', 100, '1000', 'BioVegetals', 'ES'),
(2, 'Carrots', '3', 20, '2000', 'FamilyFarm', 'IT'),
(3, 'Carrots', '2', 30, '3000', 'Carrots Farm Village', 'IT'),
(4, 'Carrots', '1', 30, '800', 'Shiny Carrots', 'FR');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country` (`country`),
  ADD KEY `producer` (`producer`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

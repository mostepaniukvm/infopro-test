<?php

class Path {

  public $currentPath;

  function __construct($path) {
    $this->currentPath = $path;
  }

  public function cd($newPath) {
    if (!$this->validate($newPath)) {
      echo "Invalid path: $newPath";
      return;
    }

    if ($newPath[0] === '/') {
      $this->currentPath = $newPath;
      return;
    }

    $current_path_components = $this->getPathComponents($this->currentPath);
    $new_path_components = $this->getPathComponents($newPath);

    foreach ($new_path_components as $index => $new_path_component) {
      switch ($new_path_component) {
        case '.':
          unset($new_path_components[$index]);
          break;

        case '..':
          // In the case if .. is not at the beginning of the string.
          if (isset($new_path_components[$index - 1])) {
            unset($new_path_components[$index - 1]);
          }
          elseif (!empty($current_path_components)) {
            array_pop($current_path_components);
          }
          unset($new_path_components[$index]);
          break;
      }
    }
    $this->currentPath = $this->buildPath(array_merge($current_path_components, $new_path_components));

  }

  /**
   * Get path components by given path string.
   *
   * @param string $path
   *   Path string.
   *
   * @return array
   *   Array of path components.
   */
  protected function getPathComponents($path) {
    $path = trim($path, '/');
    return explode('/', $path);
  }

  /**
   * Build path string by given path components.
   *
   * @param array $path_components
   *   Path components array.
   *
   * @return string
   *   Path string result.
   */
  protected function buildPath(array $path_components) {
    return '/' . implode('/', $path_components);
  }

  /**
   * Validate path string.
   *
   * @param string $path
   *   Path string.
   *
   * @return bool
   *   Validation result
   */
  protected function validate($path) {
    if (strlen($path) === strlen(utf8_decode($path))) {
      return TRUE;
    }
    return FALSE;
  }

}

$path = new Path('/a/b/c/d');
$path->cd('../.././zffsd/../gsdgg');
echo $path->currentPath;


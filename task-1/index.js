// There are few ways and here is implementation of one of them.
function solve(st, a, b) {
    var substr = st.substring(a, b + 1);
    var newSubstr = "";
    for (var i = b; i >= a; i--) {
        newSubstr += st[i];
    }
    return st.replace(substr, newSubstr);
}

var $test = solve("developer", 1, 5);
var $test2 = solve("dEVELOper", 1,5)
console.log($test);
console.log($test2);
